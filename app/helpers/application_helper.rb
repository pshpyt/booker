module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | Booker"
    end
  end
end
